import time
from thread import start_new_thread

from led_strips.configuration.environment import Environment


class LedThread:
    global_lock = None
    sleep_time = 0.5

    def __init__(self):
        self.__thread = None
        self.strip = Environment.get().strip
        self.max_brightness = 255

    def setBrightness(self, brightness=None):
        if brightness == None:
            brightness = self.max_brightness
        if brightness > self.max_brightness:
            brightness = self.max_brightness
        self.strip.setBrightness(brightness)

    def run(self, args):
        if LedThread.global_lock is not None:
            LedThread.global_lock.stop()
        LedThread.global_lock = self
        self.__thread = start_new_thread(self._run, args)

    def _run(self, *args, **kwargs):
        raise NotImplementedError()

    def is_stopped(self):
        raise NotImplementedError()

    def wait_for_is_stopped(self):
        while not self.is_stopped():
            time.sleep(LedThread.sleep_time)

    def stop(self):
        self._stop()
        LedThread.global_lock = None

    def _stop(self):
        raise NotImplementedError()
