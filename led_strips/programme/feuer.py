from neopixel import *
import random
import time

from led_strips.led_thread import LedThread


class Feuer(LedThread):
    def __init__(self):
        LedThread.__init__(self)
        self.stopped = False
        self._is_stopped = False
        self.geschwindigkeit = 50

    def is_stopped(self):
        return self._is_stopped

    def _stop(self):
        self.stopped = True
        self.wait_for_is_stopped()

    def _run(self, brightness=None):
        self.setBrightness(brightness)
        l = self.strip.numPixels()
        actColors = [[0,0,0]] * l
        for i in range(l):
            actColors[i] = [20,0,0]
        numOfNewValues = 8
        for i in range(l):
            self.strip.setPixelColor(i, Color(actColors[i][0], actColors[i][1], actColors[i][2]))
        self.strip.show()
        wait_ms = 1
        time.sleep(wait_ms/1000.0)
        while not self.stopped:
            for i in range(numOfNewValues):
                index = random.randrange(l)
                actColors[index][0] = min(actColors[index][0] + 100, 255)
                actColors[index][1] = min(actColors[index][1] + 60,255)

            oldColors = actColors
            actColors = [[0,0,0]] * l
            for i in range(l):
                act = oldColors[i]
                right = oldColors[(i+1) % l]
                left = oldColors[(i-1) % l]
                actColors[i] = [max(right[0]//6 + act[0]*2//3 + left[0]//6 - 2,0),max(right[1]//6 + act[1]*2//3 + left[1]//6 - 5, 0), 0]

            for i in range(l):
                self.strip.setPixelColor(i, Color(actColors[i][0], actColors[i][1], actColors[i][2]))
            self.strip.show()
            time.sleep(wait_ms/1000.0)
        self._is_stopped = True
