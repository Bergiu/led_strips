import time

from led_strips.led_thread import LedThread


class Leuchten(LedThread):
    def _run(self, color, brightness=None):
        self.setBrightness(brightness)
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, color)
        self.strip.show()

    def _stop(self):
        pass
