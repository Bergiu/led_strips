from flask import Flask, json, request, jsonify
from os import environ
from neopixel import Color

from led_strips.configuration.environment import Environment
from .programme.feuer import Feuer
from .programme.rotieren import Rotieren, Rotieren2
from .programme.leuchten import Leuchten

app = Flask(__name__)
strip = Environment.get().strip


def Ok():
    response = jsonify({'success':True})
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route('/aus')
def api_aus():
    no_color = Color(0, 0, 0)
    leu = Leuchten()
    leu.run((no_color, ))
    return Ok()


@app.route('/rotieren', methods=['GET'])
def api_rotieren():
    s = request.args.get('speed', type=int, default=50)
    h = request.args.get('brightness', type=int)
    rot = Rotieren(s)
    rot.run((h,))
    return Ok()


@app.route('/rotieren_2', methods=['GET'])
def api_rotieren_2():
    s = request.args.get('speed', type=float, default=3)
    h = request.args.get('brightness', type=int)
    rot = Rotieren2(s)
    rot.run((h,))
    return Ok()


@app.route('/feuer')
def api_feuer():
    feu = Feuer()
    feu.run(())
    return Ok()


@app.route('/leuchten', methods=['GET'])
def api_leuchten():
    r = request.args.get('red', type=int)
    g = request.args.get('green', type=int)
    b = request.args.get('blue', type=int)
    w = request.args.get('white', default=1, type=int)
    h = request.args.get('brightness', type=int)
    farbe = Color(r, g, b, w)
    leu = Leuchten()
    leu.run((farbe, h))
    return Ok()


@app.route('/hell')
def api_hell():
    weiss = Color(255, 255, 255)
    leu = Leuchten()
    leu.run((weiss, ))
    return Ok()


@app.route('/lila')
def api_lila():
    lila = Color(68, 0, 204)
    leu = Leuchten()
    leu.run((lila, ))
    return Ok()


if __name__ == '__main__':
    flask_host = environ.get('FLASK_HOST', "127.0.0.1")
    flask_port = environ.get('FLASK_PORT', "5000")
    app.run(host=flask_host, port=flask_port)
