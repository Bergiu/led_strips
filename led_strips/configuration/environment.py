from led_strips.configuration.strip_config import create_strip


class Environment:

    env = None

    def __init__(self, strip):
        self.strip = strip

    @staticmethod
    def create_default():
        return Environment(
            strip=create_strip()
        )

    @staticmethod
    def get():
        if Environment.env is None:
            Environment.env = Environment.create_default()
        return Environment.env