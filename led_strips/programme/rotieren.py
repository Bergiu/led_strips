
from neopixel import *
import time

from led_strips.led_thread import LedThread


class Rotieren(LedThread):
    def __init__(self, speed=50):
        LedThread.__init__(self)
        self.stopped = False
        self._is_stopped = False
        self.speed = speed

    def is_stopped(self):
        return self._is_stopped

    def _stop(self):
        self.stopped = True
        self.wait_for_is_stopped()

    def _run(self, brightness=None):
        self.setBrightness(brightness)
        RUN_COLORS = [
            (255,0,0),
            (0,255,0),
            (0,0,255),
            (255,0,0)
        ]
        l = self.strip.numPixels()
        lc = len(RUN_COLORS) - 1
        bl = l / lc
        while not self.stopped:
            for startPos in range(l):
                for i in range(l):
                    relPos = (i - startPos) % bl
                    block = (i - startPos) // bl % lc
                    factors = (relPos * 1.0 / bl, (bl - relPos) * 1.0 / bl)
                    red = RUN_COLORS[block + 1][0] * factors[0] + RUN_COLORS[block][0] * factors[1]
                    green = RUN_COLORS[block + 1][1] * factors[0] + RUN_COLORS[block][1] * factors[1]
                    blue = RUN_COLORS[block + 1][2] * factors[0] + RUN_COLORS[block][2] * factors[1]
                    color = Color(int(red), int(green), int(blue))
                    self.strip.setPixelColor(i, color)
                self.strip.show()
                time.sleep(self.speed / 1000.0)
        self._is_stopped = True


class Rotieren2(LedThread):
    def __init__(self, speed=3):
        LedThread.__init__(self)
        self.stopped = False
        self._is_stopped = False
        self.speed = speed

    def is_stopped(self):
        return self._is_stopped

    def _stop(self):
        self.stopped = True
        self.wait_for_is_stopped()

    def _run(self, brightness=None):
        self.setBrightness(brightness)
        RUN_COLORS = [
            (255,0,0),
            (0,255,0),
            (0,0,255),
            (255,0,0)
        ]
        l = int(self.strip.numPixels() * self.speed) # 30*speed
        lc = len(RUN_COLORS) - 1   # 3
        bl = l / lc                # 10
        while not self.stopped:
            for startPos in range(l):
                if self.stopped:
                    break
                relPos = (0 - startPos) % bl
                block = (0 - startPos) // bl % lc
                factors = (relPos * 1.0 / bl , (bl - relPos)*1.0 / bl)
                red = RUN_COLORS[block+1][0] * factors[0] + RUN_COLORS[block][0] * factors[1]
                green = RUN_COLORS[block+1][1] * factors[0] + RUN_COLORS[block][1] * factors[1]
                blue = RUN_COLORS[block+1][2] * factors[0] + RUN_COLORS[block][2] * factors[1]
                color = Color(int(red),int(green),int(blue))
                for i in range(self.strip.numPixels()):
                    self.strip.setPixelColor(i, color)
                self.strip.show()
                time.sleep(0.05)
        self._is_stopped = True
